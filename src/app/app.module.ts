import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { TabhomePage  } from '../pages/tabhome/tabhome';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { WebLoginPage } from '../pages/web-login/web-login';
import { SiteProgressPage } from '../pages/site-progress/site-progress';
import { SiteDiaryTabPage } from '../pages/site-diary-tab/site-diary-tab';
import { MaterialResourcePage } from '../pages/material-resource/material-resource';
import { UsertabPage } from '../pages/usertab/usertab';
import { ProjecttabPage } from '../pages/projecttab/projecttab';
import { ProgressPhotoPage } from '../pages/progress-photo/progress-photo';
import { PopoverContentPage } from '../pages/popover-content/popover-content';
import { MyprojectsPage } from '../pages/myprojects/myprojects';
import { TimesheetPage } from '../pages/timesheet/timesheet';
import { CalendarPage } from '../pages/calendar/calendar';
import { ChattabPage } from '../pages/chattab/chattab';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { FileChooser } from '@ionic-native/file-chooser';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SQLite } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
import { Push } from '@ionic-native/push';
import { FCM } from '@ionic-native/fcm';
import { Calendar } from '@ionic-native/calendar';
import { NgCalendarModule  } from 'ionic2-calendar';

import { Firebase } from '@ionic-native/firebase';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FcmProvider } from '../providers/fcm/fcm';

const firebase = {
	// your firebase web config
	apikey:"AIzaSyANvQ4yycDtzChnqejyLEZdIIMDu3Uunf4",
	projectId: "pkns-185209",
	messagingSenderId:"305973352869"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
	TabhomePage,
	LoginPage,
	ProfilePage,
	WebLoginPage,
	SiteProgressPage,
	SiteDiaryTabPage,
	MaterialResourcePage,
	UsertabPage,
	ProjecttabPage,
	ProgressPhotoPage,
	PopoverContentPage,
	MyprojectsPage,
	TimesheetPage,
	CalendarPage,
	ChattabPage
  ],
  imports: [
    NgCalendarModule,
    BrowserModule,
	HttpClientModule,
	HttpModule,
	IonicModule.forRoot(MyApp),
	AngularFireModule.initializeApp(firebase), 
    AngularFirestoreModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
	TabhomePage,
	LoginPage,
	ProfilePage,
	WebLoginPage,
	SiteProgressPage,
	SiteDiaryTabPage,
	MaterialResourcePage,
	UsertabPage,
	ProjecttabPage,
	ProgressPhotoPage,
	PopoverContentPage,
	MyprojectsPage,
	TimesheetPage,
	CalendarPage,
	ChattabPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
	File,
    Transfer,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
	BarcodeScanner,
    AuthServiceProvider,
	HttpClient,
	ScreenOrientation,
	UniqueDeviceID,
	FileChooser,
	Camera,
	PhotoViewer,
	SQLite,
	Network,
	Push,
	FCM,
	Calendar,
	Firebase,
    FcmProvider
  ]
})
export class AppModule {}
