import { Component, ViewChild } from '@angular/core';
import { Nav, Platform ,LoadingController,ToastController,Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { MenuController } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ProfilePage } from '../pages/profile/profile';
import { CalendarPage } from '../pages/calendar/calendar';

import { HomePage } from '../pages/home/home';
import { ChattabPage } from '../pages/chattab/chattab';
import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { TabhomePage  } from '../pages/tabhome/tabhome';
import { WebLoginPage } from '../pages/web-login/web-login';
import { SiteProgressPage } from '../pages/site-progress/site-progress';
import { TimesheetPage } from '../pages/timesheet/timesheet';
import { MyprojectsPage } from '../pages/myprojects/myprojects';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';

import { FcmProvider } from '../providers/fcm/fcm';


import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';

//declare var FCMPlugin: any;
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public online:boolean = true;
  profileImage:string;
  profileName: string;
  task: any;
  anotherPage: any=ProfilePage;
   loading: any;
   rootPage: any= LoginPage;
   loginData = { user:'', pass:'' ,imei: '',token:''};
  data: any;
  userId: any;
  menu:any =[];
  activePage: any;
  classes = {
  'HomePage' :HomePage,
  'TabhomePage' : TabhomePage ,
  'WebLoginPage' :WebLoginPage ,
  'SiteProgressPage' :SiteProgressPage,
  'MyprojectsPage' : MyprojectsPage,
  'TimesheetPage' :TimesheetPage,
	'CalendarPage' : CalendarPage,
	'ChattabPage': ChattabPage
}

  pages: Array<{title: string, component: any,MIcon: string}>;
  visitorForInsertOnServer: any;
  manpowerForInsertOnServer : any;
  machineryForInsertOnServer : any;
  materialForInsertOnServer : any;
 
  constructor(public events:Events,public fcm: FcmProvider,public menuCtrl: MenuController,public authServiceProvider: AuthServiceProvider,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public loadingCtrl: LoadingController, private toastCtrl: ToastController,private screenOrientation: ScreenOrientation, private sqlite: SQLite, private network: Network) {
    this.initializeApp();
    this.platform.ready().then((readySource) => {
			// Get a FCM token
      fcm.getToken()

      // Listen to incoming messages
      fcm.listenToNotifications().pipe(
        tap(msg => {
          // show a toast
          const toast = toastCtrl.create({
            message: msg.body,
            duration: 3000
          });
          toast.present();
        })
      )
      .subscribe()
    

    
	 let type = this.network.type;
			  
	if(this.platform.width()<567)
	 this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
	else
	  this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
		
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: "HomePage" ,MIcon:"home-icon.png"}
     
    ];
	if (this.platform.width()<567) {      
    	 this.menuCtrl.close();
   }
   else{
	    this.menuCtrl.open();
   }
	 this.activePage = this.pages[0];
	var storage = window.localStorage;
	//storage.setItem("login","false");
	 let val=storage.getItem("login");
	 //alert(val);
     if(type == "unknown" || type == "none" || type == undefined){
				this.presentToast("wait");	
				this.online = false;
				if(val === "true"){
				   this.getSqlLiteData();
				}
				
	 }else{
				console.log("The device is connected to internet!");
				this.online = true;
				if(val === "true"){
					  this.loginData.user=storage.getItem("l_username");
					  this.loginData.pass=storage.getItem("l_pass");
					  this.loginData.imei=storage.getItem("imei");
					   this.signin() ;
		
	            }
          }
	}); 
	 this.task = setInterval( () => {
	  this.networkCheck();
	}, 3000);
	this.listenEvents();
	

	//this. profileImage="assets/img/logo.png";
	// alert(this. profileImage);
	//this. profileImage= storage.getItem("profile");
  }

   listenEvents(){
   this.events.subscribe('reloadProfile',(img) => {
    //call methods to refresh content
	// alert('profile image get updated.');
	var storage = window.localStorage;
	this. profileImage= img;
	//this.profileName="snehal";
   });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
	   this.sqlite.create({
				name: "projectList.db",
				location: "default"
			})
				.then((db: SQLiteObject) => {
				
                 this.createTable(db);
				
			}, (error) => {
				console.error("Unable to open database", error);
			});
		});
      
  }

  createTable(db: SQLiteObject) {
      db.executeSql("CREATE TABLE IF NOT EXISTS projects (ProjectID INTEGER, ProjectName TEXT)", {}).then((data) => {}, (error) => {});
	  db.executeSql("CREATE TABLE IF NOT EXISTS Menu (title TEXT, component TEXT)", {}).then((data) => {}, (error) => {});
      db.executeSql("CREATE TABLE IF NOT EXISTS visitors (id INTEGER PRIMARY KEY AUTOINCREMENT,ProjectID INTEGER, dt TEXT,name TEXT, company TEXT, inData TEXT, outData TEXT, actionData TEXT)", {}).then((data) => {}, (error) => {});
	  db.executeSql("CREATE TABLE IF NOT EXISTS ManPowerResource (ResourceID INTEGER, resource TEXT)", {}).then((data) => {}, (error) => {});
	  db.executeSql("CREATE TABLE IF NOT EXISTS ManPower (id INTEGER PRIMARY KEY AUTOINCREMENT,ProjectID INTEGER, dt TEXT,resource TEXT, Qty TEXT, remark TEXT)", {}).then((data) => {}, (error) => {});
	  db.executeSql("CREATE TABLE IF NOT EXISTS MachineryResource (ResourceID INTEGER, resource TEXT)", {}).then((data) => {}, (error) => {});
      db.executeSql("CREATE TABLE IF NOT EXISTS Machinery (id INTEGER PRIMARY KEY AUTOINCREMENT,ProjectID INTEGER, dt TEXT,resource TEXT, Qty TEXT, remark TEXT)", {}).then((data) => {}, (error) => {});
	  db.executeSql("CREATE TABLE IF NOT EXISTS Material (id INTEGER PRIMARY KEY AUTOINCREMENT,ProjectID INTEGER, dt TEXT,resource TEXT, brand TEXT,Qty TEXT, remark TEXT)", {}).then((data) => {}, (error) => {});
  }


  openProfile(){
   this.menuCtrl.close();
    this.nav.setRoot(this.anotherPage);
  }
  openPage(page) {
  // alert(page);
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
   if (this.platform.width()<567) {      
    	 this.menuCtrl.close();
   }
   else{
	    this.menuCtrl.open();
   }
    this.nav.setRoot(this.classes[page]);
  }

  checkSize(){
    if (this.rootPage == LoginPage){
     return false;
    }
	else{
	 if (this.platform.width()<567){      
	     return false;
	 }
     else{
	   this.pages[0] =   { title: 'Home', component: "TabhomePage" , MIcon:"home-icon.png"};
	   return true;
	  }
	}
  }

   showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }

   signin() {
    var storage = window.localStorage;
    this.showLoader('Authenticating...');
    this.authServiceProvider.login(this.loginData).then((result) => {
	 
      this.loading.dismiss();
	
      this.data = result;
	  //  alert(this.data.Login);
      if(this.data.Login== true){
		
	    if(this.data.Active== true){
		  this.getMenu();
	      if (this.platform.width()<567)      
           this.rootPage=HomePage; 
	      else
		   this.rootPage=TabhomePage;
	   }
	   else{
	       if( storage.getItem("activate")!='true')
	       { 
		     storage.setItem("activate", "false");
	       }
		   else
		      this.presentToast("User is not activated.");
		    this.nav.setRoot(LoginPage); 
		 }
		 
      }
      else{
	    this.nav.setRoot(LoginPage); 
	     //alert("error");
       
      }
    }, (err) => {
      this.loading.dismiss();
	  this.getSqlLiteData(); 
	  alert("Server have some issue, please try after some time");
    //  this.presentToast(err);
	  
    });
  }


   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 10000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
 
 getMenu(){
    var storage = window.localStorage;
    this.showLoader("Loading...");
    this.authServiceProvider.GetMenu(this.loginData).then((result) => {
	 
      this.loading.dismiss();
	
      this.data = result;
	  this.menu=this.data.lstUserMenu;
	  storage.setItem("profile", this.data.objUserInfo1.ProfilePicture);
	  storage.setItem("name",  this.data.objUserInfo1.UserName);
	  for (var i=1;i<this.menu.length;i++){
	     this.pages.push({ title:this.menu[i].MMenu , component: this.menu[i].MLocation ,MIcon:this.menu[i].MIcon });
	  }
	  this.profileImage=this.data.objUserInfo1.ProfilePicture+"?"+this.createFileName();
	  this.profileName=this.data.objUserInfo1.UserName;
	 //  alert(this.profileImage);
	 this.insertMenuInSqlLiteDb();
	}, (err) => {
      this.loading.dismiss();
	  // this.rootPage=LoginPage; 
	  alert("err"+err);
    //  this.presentToast(err);
    });
  }

   // Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
  checkActive(page) {
     return page == this.activePage;
  }

   insertMenuInSqlLiteDb() {

      this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM Menu", {})
        .then(() => console.log('All records deleted'))
        .catch(e => console.log(e));
		
	 });

	 // this.presentLoadingDefault();
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 

      for(var i= 0; i<=this.menu.length; i++) {
		var title = (JSON.stringify(this.menu[i].title));
        var component = (JSON.stringify(this.menu[i].component));
	       db.executeSql('INSERT INTO Menu(title, component) VALUES(?, ?)', [this.menu[i].MMenu,  this.menu[i].MLocation])
		           .then(() => console.log('Executed SQL'))
		          .catch(e => alert(e));
       }
	 }); 
  }

  getSqlLiteData() {
       var storage = window.localStorage;
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from Menu', {}).then((data) => {
		 
		for (var i=1;i<data.rows.length;i++){
	      this.pages.push({ title:data.rows.item(i).title , component: data.rows.item(i).component ,MIcon:this.menu[i].MIcon });
	    }

		this.profileImage= storage.getItem("profile");
	    this.profileName= storage.getItem("name");
		 // alert(this.pages.length);			
		 if (this.platform.width()<567)      
           this.rootPage=HomePage; 
	      else
		   this.rootPage=TabhomePage; 
	    });
	  });
   }



   networkCheck(){

     let type = this.network.type;

	  if(type == "unknown" || type == "none" || type == undefined){
		 console.log("The device is not connected to internet!");		
		this.online = false;			 
	  }else{
		//alert("The device is connected to internet!");
		this.online = true;
		this.siteDatafetchFromServer();
		this.manpowerDatafetchFromServer();
		this.machineryDatafetchFromServer();
		this.materialDatafetchFromServer();
	  }

		this.network.onConnect().subscribe(data => {
              this.online = true;
			console.log(data)
		  }, error => console.log(error));
		 
		 this.network.onDisconnect().subscribe(data => {
			 this.online = false;
			console.log(data)
		  }, error => console.log(error));
  }

   siteDatafetchFromServer() {
      this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from visitors', {}).then((data) => {

		 this.visitorForInsertOnServer = [];
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {	
			//alert(data.rows.item(i).dt);
			this.visitorForInsertOnServer.push({projectId:data.rows.item(i).ProjectID,date: data.rows.item(i).dt,name: data.rows.item(i).name, company: data.rows.item(i).company, in: data.rows.item(i).inData, out: data.rows.item(i).outData});
			}
			
			this.insertDataOnserver();
		  }
	    });

	  }); 
  
  }
 


  insertDataOnserver() {
   for(var i = 0; i < this.visitorForInsertOnServer.length; i++) {
			 var url="fType=SSV&fName="+this.visitorForInsertOnServer[i].name+"&comments="+this.visitorForInsertOnServer[i].company+"&IN="+this.visitorForInsertOnServer[i].in+"&OUT="+this.visitorForInsertOnServer[i].out+"&SDate="+this.visitorForInsertOnServer[i].date+"&PID="+this.visitorForInsertOnServer[i].projectId;
           // alert(url);
			this.authServiceProvider.getURLData(url).subscribe(
			 result => {
			  this.presentToast('Data inserted on server!'); 
			},
			err =>{
			  alert("Error : "+err);
			} ,
			() => {
			 // alert('getData completed');
			}
		  );
	}
	 this.deleteTableData();
  }
 manpowerDatafetchFromServer() {
      this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from ManPower', {}).then((data) => {

		 this.manpowerForInsertOnServer = [];
		// alert(data.rows.length);
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {	
			//alert(data.rows.item(i).resource);
			this.manpowerForInsertOnServer.push({projectId:data.rows.item(i).ProjectID,date: data.rows.item(i).dt,resource: data.rows.item(i).resource, Qty: data.rows.item(i).Qty, remark: data.rows.item(i).remark});
			}
			
			this.manpowerinsertDataOnserver();
		  }
	    });

	  }); 
  
  }
  manpowerinsertDataOnserver(){
    for(var i = 0; i < this.manpowerForInsertOnServer.length; i++) {
			 var url="fType=SSR&ItemID="+this.manpowerForInsertOnServer[i].resource+"&Qty="+this.manpowerForInsertOnServer[i].Qty+"&comments="+this.manpowerForInsertOnServer[i].remark+"&SDate="+this.manpowerForInsertOnServer[i].date+"&PID="+this.manpowerForInsertOnServer[i].projectId;
          //  alert(url);
			this.authServiceProvider.getURLData(url).subscribe(
			 result => {
			  this.presentToast('Data inserted on server!'); 
			},
			err =>{
			  alert("Error : "+err);
			} ,
			() => {
			 // alert('getData completed');
			}
		  );
	}

	 this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM ManPower", {})
        .then(() => console.log('All records deleted'))
        .catch(e => console.log(e));
		
	 });
  }

  machineryDatafetchFromServer() {
      this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from Machinery', {}).then((data) => {

		 this.machineryForInsertOnServer = [];
		// alert(data.rows.length);
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {	
			//alert(data.rows.item(i).resource);
			this.machineryForInsertOnServer.push({projectId:data.rows.item(i).ProjectID,date: data.rows.item(i).dt,resource: data.rows.item(i).resource, Qty: data.rows.item(i).Qty, remark: data.rows.item(i).remark});
			}
			
			this.machineryinsertDataOnserver();
		  }
	    });

	  }); 
  
  }

  machineryinsertDataOnserver(){
    for(var i = 0; i < this.machineryForInsertOnServer.length; i++) {
			 var url="fType=SSM&ItemID="+this.machineryForInsertOnServer[i].resource+"&Qty="+this.machineryForInsertOnServer[i].Qty+"&comments="+this.machineryForInsertOnServer[i].remark+"&SDate="+this.machineryForInsertOnServer[i].date+"&PID="+this.machineryForInsertOnServer[i].projectId;
           // alert(url);
			this.authServiceProvider.getURLData(url).subscribe(
			 result => {
			  this.presentToast('Data inserted on server!'); 
			},
			err =>{
			  alert("Error : "+err);
			} ,
			() => {
			 // alert('getData completed');
			}
		  );
	}

	 this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM Machinery", {})
        .then(() => console.log('All records deleted'))
        .catch(e => console.log(e));
		
	 });
  }


   materialDatafetchFromServer() {
      this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from Material', {}).then((data) => {

		 this.materialForInsertOnServer = [];
		// alert(data.rows.length);
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {	
			//alert(data.rows.item(i).resource);
			this.materialForInsertOnServer.push({projectId:data.rows.item(i).ProjectID,date: data.rows.item(i).dt,resource: data.rows.item(i).resource, brand:data.rows.item(i).brand, Qty: data.rows.item(i).Qty, remark: data.rows.item(i).remark});
			}
			
			this.materialinsertDataOnserver();
		  }
	    });

	  }); 
  
  }

  materialinsertDataOnserver(){
  var storage = window.localStorage;
				 
	var uid= storage.getItem("l_username");

    for(var i = 0; i < this.materialForInsertOnServer.length; i++) {
			 var url="fType=SST&CH="+this.materialForInsertOnServer[i].resource+"&AKK="+this.materialForInsertOnServer[i].brand+"&Qty="+this.materialForInsertOnServer[i].Qty+"&comments="+this.materialForInsertOnServer[i].remark+"&SDate="+this.materialForInsertOnServer[i].date+"&PID="+this.materialForInsertOnServer[i].projectId+"&uid="+uid;
           // alert(url);
			this.authServiceProvider.getURLData(url).subscribe(
			 result => {
			  this.presentToast('Data inserted on server!'); 
			},
			err =>{
			  alert("Error : "+err);
			} ,
			() => {
			 // alert('getData completed');
			}
		  );
	}

	 this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM Material", {})
        .then(() => alert('All records deleted'))
        .catch(e => console.log(e));
		
	 });
  }

  deleteTableData() {
     this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM visitors", {})
        .then(() => console.log('All records deleted'))
        .catch(e => console.log(e));
	 });

	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM projects", {})
        .then(() => alert('All records deleted'))
        .catch(e => console.log(e));
		
	 });
  }


  
}
