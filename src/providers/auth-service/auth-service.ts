//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

let apiUrl = 'http://103.253.109.23/pkns.web/mobile/webservice?';
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  constructor(public http: Http) {
    console.log('Hello AuthServiceProvider Provider');
 }
 
 public login(credentials) {
    let url=apiUrl+'fType=A&uid='+credentials.user+'&pwd='+credentials.pass+'&imei='+credentials.imei;
	 console.log(url);
	//alert(url);
    return new Promise((resolve, reject) => {
     
        this.http.get(url)
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }
 
 public activation(credentials) {
    let url=apiUrl+'fType=MR&uid='+credentials.user+'&pwd='+credentials.pass+'&gcm='+credentials.token+'&imei='+credentials.imei;
	//alert(url);

    return new Promise((resolve, reject) => {
     
        this.http.get(url)
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }


  public GetMenu(credentials) {
    let url=apiUrl+'fType=M&uid='+credentials.user;
	//alert(url);
	console.log("Menu"+url);
    return new Promise((resolve, reject) => {
     
        this.http.get(url)
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }
  public getProjectList(user){
    let url=apiUrl+'fType=P&uid='+user;
	console.log(url);
	//alert(url);
    return new Promise((resolve, reject) => {
     
        this.http.get(url)
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
   
  }
   public getData(type,user) {
    let url=apiUrl+'fType='+type +'&uid='+user;
	console.log(url);
	//alert(url);
    return new Promise((resolve, reject) => {
     
        this.http.get(url)
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }


getCount(eDate,eYear,user){
  // alert(eDate);
  let url=apiUrl+'fType=FCN&uid='+user+'&month='+eDate+'&year='+eYear;
	 return this.http.get(url).map(res => res.json());
  }


 /* setSiteData(siteProgressData){
     
  let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
	let data=JSON.stringify(siteProgressData);
	let options = new RequestOptions({headers: headers})

	return this.http.post('http://skylimit.co.in/Client/ChatApplication/setSiteProgress.php?',data,options).map(res => res.json());
  }*/
   
   setQRCode(type, user,qrcode){
    console.log("url is"+apiUrl+'fType='+type+'&uid='+user+'&qrcode='+qrcode);
     return this.http.get(apiUrl+'fType='+type+'&uid='+user+'&qrcode='+qrcode).map(res => res.json());
  }

  getDataByType(type){
    return this.http.get(apiUrl+'fType='+type).map(res => res.json());
  }

  getURLData(url){
    console.log("url is"+apiUrl+url);
    return this.http.get(apiUrl+url).map(res => res.json());
  }

setpunchTime(url){
 //  console.log(apiUrl+url);
    return this.http.get(url).map(res => res.json());
  }
   getVisitorData(id,dt) {
     
    let url="fType=FSV&pid="+id+"&SDate="+dt;
	 console.log(apiUrl+url);
    return this.http.get(apiUrl+url).map(res => res.json());
   }


   imageListing() {
    return new Promise((resolve, reject) => {
        this.http.get('http://skylimit.co.in/Client/ChatApplication/getPhotoList.php')
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }

/*  saveProgressImage(credentials) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.http.post(apiUrl+'upload.php?', JSON.stringify(credentials), {headers: headers})
          .subscribe(res => {
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
    });
  }*/

  public getCalendarData () {
    let url=apiUrl+'fType=FCN&uid=K1462&month=3&year=2018';
	 console.log("url is"+url);
	//alert(url);
  return new Promise((resolve, reject) => {
     
    this.http.get(url)
      .subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
});
          
  
  }
}
