import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import * as moment from 'moment';
/**
 * Generated class for the CalendarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
    date: any;
    daysInThisMonth: any;
    daysInLastMonth: any;
    daysInNextMonth: any;
    monthNames: string[];
    currentMonth: any;
    currentYear: any;
    currentDate: any;
    eventList: any;
    selectedEvent: any;
    isSelected: any;
    projectData:any;
    event:any;
    constructor(private navController:NavController,public authServiceProvider: AuthServiceProvider) {
        this.getCalendarList();
    }

/******************get data***************************/
ionViewWillEnter() {
    this.date = new Date();
    this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    this.getDaysOfMonth();
    
  }

  getCalendarList(){
    console.log("result is called");
    this.authServiceProvider.getCalendarData().then((result) => {
      console.log("result is"+result);
     // this.loading.dismiss();
	  this.projectData = result;
    console.log("result is"+this.projectData[0].SDate);
	}, (err) => {
     // this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }

  getDaysOfMonth() {
  this.daysInThisMonth = new Array();
  this.daysInLastMonth = new Array();
  this.daysInNextMonth = new Array();
  this.currentMonth = this.monthNames[this.date.getMonth()];
  this.currentYear = this.date.getFullYear();
  if(this.date.getMonth() === new Date().getMonth()) {
    this.currentDate = new Date().getDate();
  } else {
    this.currentDate = 999;
  }

  var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
  var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
  for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
    this.daysInLastMonth.push(i);
  }

  var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
  for (var i = 0; i < thisNumOfDays; i++) {
    this.daysInThisMonth.push(i+1);
  }

  var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
  var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
  for (var i = 0; i < (6-lastDayThisMonth); i++) {
    this.daysInNextMonth.push(i+1);
  }
  var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
  if(totalDays<36) {
    for(var i = (7-lastDayThisMonth); i < ((7-lastDayThisMonth)+7); i++) {
      this.daysInNextMonth.push(i);
    }
  }
}

goToLastMonth() {
  this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
  this.getDaysOfMonth();
}

goToNextMonth() {
  this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
  this.getDaysOfMonth();
}



/*loadEventThisMonth() {
    this.eventList = new Array();
    var startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
    var endDate = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0);
    this.calendar.listEventsInRange(startDate, endDate).then(
      (msg) => {
        msg.forEach(item => {
          this.eventList.push(item);
        });
      },
      (err) => {
        console.log("error"+err);
      }
    );
  }*/


  checkEvent(day) {
    var hasEvent = false;

  /*  var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
    var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
    this.eventList.forEach(event => {
      if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        hasEvent = true;
      }
    });*/
    var thisDate1 = moment(this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day).format( 'YYYY-MM-DD' );
   // var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
    for(var i= 0; i<this.projectData.length; i++) {
    //  console.log("result is"+this.projectData[i].SDate);
      var time= moment(this.projectData[i].SDate).format( 'YYYY-MM-DD' );
      console.log("current date"+thisDate1);
      console.log("data date"+time);
      if(thisDate1==time){
        this.event=(this.projectData[i].Count);
        hasEvent=true;
      }
     
    }
    
    return hasEvent;
  }

  selectDate(day) {
    this.isSelected = false;
    this.selectedEvent = new Array();
  /*  var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
    var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
    this.eventList.forEach(event => {
      if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
        this.isSelected = true;
        this.selectedEvent.push(event);
      }
    });*/
    var thisDate1 = moment(this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day).format( 'YYYY-MM-DD' );
    // var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
     for(var i= 0; i<this.projectData.length; i++) {
     //  console.log("result is"+this.projectData[i].SDate);
       var time= moment(this.projectData[i].SDate).format( 'YYYY-MM-DD' );
       console.log("current date"+thisDate1);
       console.log("data date"+time);
       if(thisDate1==time){
        this.selectedEvent=this.projectData[i].lstCalNotDet;//push({ID:'',Type:'',Activiy:'',Status:'',SDate:'',Day:''});
        this.isSelected = true;
      
       }
      }
  }

 


}
