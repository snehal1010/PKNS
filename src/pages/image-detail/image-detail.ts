import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-image-detail',
  templateUrl: 'image-detail.html',
})
export class ImageDetailPage {

  image: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

	this.image = this.navParams.get("image");
    console.log(this.image);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImageDetailPage');
  }

}
