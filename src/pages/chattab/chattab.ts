import { Component ,ViewChild} from '@angular/core';
import { IonicPage, Tabs,NavController, NavParams ,Platform} from 'ionic-angular';
import { UsertabPage } from '../usertab/usertab';
import { ProjecttabPage } from '../projecttab/projecttab';

/**
 * Generated class for the ChattabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chattab',
  templateUrl: 'chattab.html',
 
})
export class ChattabPage {
  @ViewChild('chatTabs') tabRef: Tabs;
  Chattab1=UsertabPage;
  Chattab2=ProjecttabPage;
  Chattab3=ProjecttabPage;
  
  constructor(public platform: Platform,public navCtrl: NavController, public navParams: NavParams) {
    // this.tabRef.select(0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChattabPage');
  }

}
