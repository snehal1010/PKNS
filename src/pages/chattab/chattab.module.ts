import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChattabPage } from './chattab';

@NgModule({
  declarations: [
    ChattabPage,
  ],
  imports: [
    IonicPageModule.forChild(ChattabPage),
  ],
})
export class ChattabPageModule {}
