import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SiteProgressPage } from './site-progress';

@NgModule({
  declarations: [
    SiteProgressPage,
  ],
  imports: [
    IonicPageModule.forChild(SiteProgressPage),
  ],
})
export class SiteProgressPageModule {}
