import { ViewChild } from '@angular/core';
import { Nav,Tabs, Events } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController, Platform, ToastController} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { SiteDiaryTabPage } from '../site-diary-tab/site-diary-tab';
import { MaterialResourcePage } from '../material-resource/material-resource';
import { ProgressPhotoPage } from '../progress-photo/progress-photo';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
/**
 * Generated class for the SiteProgressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-progress',
  templateUrl: 'site-progress.html',
   
})
export class SiteProgressPage {
@ViewChild('myTabs') tabRef: Tabs;
tab:Tabs;
public online:boolean = true;
 projectData: any;
 loading: any;
  tab1=SiteDiaryTabPage;
  tab2=MaterialResourcePage;
  tab3=ProgressPhotoPage;
  tab4= SiteDiaryTabPage;
  activeTab:string="0";


  tabParam = { project:'', date:''};
  public myDate: string = new Date().toISOString();
  constructor(public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController,private sqlite: SQLite, private network: Network, private platform: Platform, public toastCtrl: ToastController) {
    var storage = window.localStorage;
   storage.setItem("project","");
   storage.setItem("dt","");
   this.platform.ready().then( () => {
           let type = this.network.type;

			  if(type == "unknown" || type == "none" || type == undefined){
				this.presentToast();	
				this.online = false;
				this.getSqlLiteData();
			  }else{
				console.log("The device is connected to internet!");
				this.online = true;
				this.getProjectList();
			  }
			});
		   
		 this.network.onConnect().subscribe(data => {
              this.online = true;
			console.log(data)
		  }, error => console.log(error));
		 
		 this.network.onDisconnect().subscribe(data => {
			 this.online = false;
			console.log(data)
		  }, error => console.log(error));
  }

  getSqlLiteData() {
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from projects', {}).then((data) => {

		 this.projectData = [];
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {
			   this.projectData.push({ProjectName: data.rows.item(i).ProjectName, ProjectID: data.rows.item(i).ProjectID});
			}			
		  }
		  else {
			 alert("You forgot to synchronise data.");
		  }
		  
	    });
	  });
   }

   presentToast() {
	let toast = this.toastCtrl.create({
		message: 'The device is not connected to internet!',
		duration: 3000,
		position: 'middle'
	});

	toast.onDidDismiss(() => {
	 console.log('Dismissed toast');
	});

	toast.present();
   }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SiteProgressPage');
  }

   getProjectList(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.getProjectList(storage.getItem("l_username")).then((result) => {
	 
      this.loading.dismiss();
	  this.projectData = result;
	
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }

	fabButtonClick() {
	   this.navCtrl.push('SiteFormPage');
	}

   showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }

  
   change(selectedValue: any){
   
	var storage = window.localStorage;
	let dt = new Date(this.myDate);    
	let dd = dt.getDate();
    let mm = dt.getMonth()+1; 
    let yr = dt.getFullYear();
	var dd1=dd.toString();
	if(dd1.length<2)
	  dd1="0"+dd;
	  storage.setItem("dt", dd1+"/"+mm+"/"+yr);
      storage.setItem("project",selectedValue);
	  let tab:any = this.tabRef.getSelected();//Returns the currently selected tab
  // alert(dd1);
  // this.activeTab=tab.index;

      this.tabRef.select(tab.index);
	// this.navCtrl.setRoot(this.navCtrl.getActive().component);
   }
  

   changedate(dt:any){
      var yr=dt.year;
	  var dd=dt.day;
	  var mt=dt.month;
	
	  var dd1=dd.toString();
	  var mm1=mt.toString();
	  if(dd1.length<2)
	      dd1="0"+dd;
      if(mm1.length<2)
	     mm1="0"+mt;
	
	    this.tabParam.date=dd1+"/"+mm1+"/"+yr; 
	  var storage = window.localStorage;
	  storage.setItem("dt", this.tabParam.date);
   
	  let tab:any = this.tabRef.getSelected();//Returns the currently selected tab
   //alert(this.tabParam.date);
  // this.activeTab=tab.index;

      this.tabRef.select(tab.index);
   }

}
