import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,ToastController,Platform } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the MachineryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-machinery',
  templateUrl: 'machinery.html',
})
export class MachineryPage {
    RescData:any;
    machineryData ={resource:'', qty:'', remark:''};
    public online:boolean = true; 

  constructor(public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider, private sqlite: SQLite, private network: Network, private toastCtrl: ToastController, private platform: Platform, public events:Events) {
  this.platform.ready().then( () => {
           let type = this.network.type;

			  if(type == "unknown" || type == "none" || type == undefined){
				this.presentToast();	
				this.online = false;
				this.getSqlLiteData();
			  }else{
				console.log("The device is connected to internet!");
				this.online = true;
			    this.getRescData();
			  }
			});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MachineryPage');
  }

  getRescData(){
   var url="fType=MM";
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.RescData=result;
     
	}, (err) => {
     
	  alert("err"+JSON.stringify(err));
    });
  }

  getSqlLiteData() {
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from MachineryResource', {}).then((data) => {

		 this.RescData = [];
		 if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {
			   this.RescData.push({MID: data.rows.item(i).ResourceID, MDescription: data.rows.item(i).resource});
			}			
		  }
		  else {
			 alert("You forgot to synchronise data.");
		  }
		  
	    });
	  });
   }

   submitData() {
     var storage = window.localStorage;
				 
	
    var project= storage.getItem("project");

   if(project === "")
	    alert("Select any project");
	  else{
      let type = this.network.type;

		  if(type == "unknown" || type == "none" || type == undefined){
			this.presentToast();
			this.online = false;
			this.submitFormDataOnSqlLiteDb();
			
		  }else{
			console.log("The device is connected to internet!");
			this.online = true;
			this.submitFormDataOnServer();
		  }
       }
  }

  submitFormDataOnServer() {
  
   var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");

   var url="fType=SSM&ItemID="+this.machineryData.resource+"&Qty="+this.machineryData.qty+"&comments="+this.machineryData.remark+"&SDate="+date+"&PID="+project;
  // alert(url);
    this.authServiceProvider.getURLData(url).subscribe(
     result => {
	 this.events.publish('reloadDetails');
	   this.navCtrl.pop();
	 
    },
    err =>{
      alert("Error : "+err);
    } ,
    () => {
     // alert('getData completed');
    }
  );
 }


 submitFormDataOnSqlLiteDb() {
    var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => {
		  db.executeSql('INSERT INTO Machinery(ProjectID,dt,resource, Qty, remark) VALUES(?, ?, ?, ?,?)', [project,date,this.machineryData.resource,  this.machineryData.qty, this.machineryData.remark])
		  .then(() => alert('Data Inserted in SQLLite DB'))
		  .catch(e => alert(e));
		
	       this.navCtrl.pop();
       });
   }

 presentToast() {
	let toast = this.toastCtrl.create({
	message: 'The device is not connected to internet!',
	duration: 3000,
	position: 'middle'
	});

	toast.onDidDismiss(() => {
	console.log('Dismissed toast');
	});

	toast.present();
  }
}
