import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,LoadingController, Loading,ToastController,Events} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import {Camera} from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular'
import { Headers,RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
  declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [
    Camera,
    
	Transfer, TransferObject, File
  ]
})
export class ProfilePage {
 public base64Image: string;
  options:any;
  imageURL: string;
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  public data : any = [];
  lastImage: string = null;
  TrId:string;
  name:string;
  Designation:string;
  Mobile:string;
  Email:string;
  public online:boolean = true;
  loading: Loading;
 constructor(public events:Events,public navCtrl: NavController, public navParams: NavParams,private camera: Camera,public actionSheetCtrl: ActionSheetController, private transfer: Transfer, private file: File, private filePath: FilePath,public platform: Platform,public loadingCtrl: LoadingController, public toastCtrl: ToastController,public authServiceProvider: AuthServiceProvider, private network: Network) {
  this.platform.ready().then((readySource) => {
	 let type = this.network.type;
	  if(type == "unknown" || type == "none" || type == undefined){
				this.presentToast("No intenet to display details");
				this.online = false;
				
				
	 }else{
				console.log("The device is connected to internet!");
				this.online = true;
				this.getUserInfo();
    }
	}); 
     var storage = window.localStorage;
	 this.base64Image= storage.getItem("profile")+"?"+this.createFileName();
	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
   getUserInfo(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.getData("UI",storage.getItem("l_username")).then((result) => {
	 
      this.loading.dismiss();
	
      this.data = result;
	  this.TrId=this.data.TrID;
	  this.name=this.data.UserName;
      this.Designation=this.data.Designation;
      this.Mobile=this.data.Mobile;
      this.Email=this.data.Email;
	 
	 // this.base64Image=this.data.UserGroup;
	}, (err) => {
      this.loading.dismiss();
	  // this.rootPage=LoginPage; 
	  alert("err"+err);
    //  this.presentToast(err);
    });
  }

  showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }
 
openActionSheet(){
 console.log('opening');
 let actionsheet = this.actionSheetCtrl.create({
 title:"Choose Album",
 buttons:[{
 text: 'Camera',
 handler: () => {
  this.takePicture(this.camera.PictureSourceType.CAMERA);
 
 }
 },{
 text: 'Gallery',
 handler: () =>{

  this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
 }
 }]
 });
 actionsheet.present();
}


public takePicture(sourceType) {
  // Create options for the Camera Dialog
  var options = {
    quality: 100,
	targetWidth: 600,
	targetHeight: 600,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation: true
  };
 
  // Get the data of an image
  this.camera.getPicture(options).then((imagePath) => {
   //alert(imagePath);
    // Special handling for Android library
    if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
	   
      this.filePath.resolveNativePath(imagePath)
        .then(filePath => {
		  //alert(filePath);
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
		 
          this.copyFileToLocalDir(correctPath, currentName, this.TrId+ ".jpg");
        }, (err) => {
    alert(JSON.stringify(err));});
    } else {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
	   
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
	
  }, (err) => {
    alert('Error while selecting image.');
  });
}

	

 // Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
 
// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
	//alert(" dir"+cordova.file.dataDirectory);
	this.base64Image=namePath+currentName;
	this.uploadImage();
	//this.fileChange;
  }, error => {
    alert('Error while storing file.');
  });

  
}
 

 
// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
var win = function (r) {
        alert("Code = " + r.responseCode);
        alert("Response = " + r.response);
        alert("Sent = " + r.bytesSent);
    };

    var fail = function (error) {
        alert("An error has occurred: Code = " + error.code);
        alert("upload error source " + error.source);
        alert("upload error target " + error.target);
    };

var storage = window.localStorage;
 
  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);
 
  // File name only
  var filename = this.lastImage;
  // Destination URL
 
  var url = "http://103.253.109.23/pknsv1.web/mobile/webservice?fType=PP&uid=" +storage.getItem("l_username")+"&fName="+filename;
   alert(url);
  var options  = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "image/jpeg",
    headers: {}
	
  };
 
  const fileTransfer: TransferObject = this.transfer.create();
 
  this.loading = this.loadingCtrl.create({
    content: 'Uploading...',
  });
  this.loading.present();
  var storage = window.localStorage;
  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath,url, options).then(data => {
    this.loading.dismissAll();
	// storage.setItem("profile", this.base64Image);
	this.events.publish('reloadProfile',this.base64Image);
    alert('Image succesfully uploaded.');
  }, err => {
    this.loading.dismissAll()
   alert(JSON.stringify(err));
  });
}

download() {
 const fileTransfer: TransferObject = this.transfer.create();
 const url = this.base64Image;
 fileTransfer.download(url, this.file.dataDirectory + 
'file.pdf').then((entry) => {
 alert('download complete: ' + entry.toURL());
}, (error) => {
alert(error);
});
}

 presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 30000,
      position: 'middle',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
