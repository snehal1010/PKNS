import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgressPhotoPage } from './progress-photo';

@NgModule({
  declarations: [
    ProgressPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgressPhotoPage),
  ],
})
export class ProgressPhotoPageModule {}
