import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController  } from 'ionic-angular';
import { Camera, CameraOptions  } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-progress-photo',
  templateUrl: 'progress-photo.html',
})
export class ProgressPhotoPage {
   
  base64Image: any;
  imageData = { image :''};
  image: any;
 // progressImages: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public camera:Camera,public actionSheetCtrl: ActionSheetController, private photoViewer: PhotoViewer, public authServiceProvider: AuthServiceProvider, public toastCtrl: ToastController) {

	  this.progressImageListing();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgressPhotoPage');
  }

  selectFromGallery() {

	this.camera.getPicture({
	 quality: 60,
     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
     destinationType: this.camera.DestinationType.DATA_URL,

    }).then((imageData) => {
      this.base64Image = imageData;
	  //alert(this.base64Image);
	    this.imageData = { image :this.base64Image};
      this.saveImage();
	  this.presentToast();
	  }, (err) => {
      console.log(err);
    });
		
  }
 
  openCamera() {

	this.camera.getPicture({
	 quality: 60,
     sourceType: this.camera.PictureSourceType.CAMERA,
     destinationType: this.camera.DestinationType.DATA_URL,
	 
    }).then((imageData) => {
      this.base64Image = imageData;
	  //alert(this.base64Image);
	   this.imageData = { image :this.base64Image};
      this.saveImage();
	    this.presentToast();
	  }, (err) => {
      console.log(err);
    });
		 
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose or take a picture',
      buttons: [
        {
          text: 'Take a picture',
          handler: () => {
            this.openCamera();
		
          }
        },
        {
          text: 'Choose pictures',
          handler: () => {
            this.selectFromGallery();
			
          }
        }
      ]
    });
    actionSheet.present();
  }

  public progressImages: any = [];

  progressImageListing() {
    this.authServiceProvider.imageListing().then((result) => {
	// alert(result);
      this.progressImages = result;
	  
    }, (err) => {
       alert("Error");
    });
  
  }
   
   saveImage() {
   /* this.authServiceProvider.saveProgressImage(this.imageData).then((result) => {
	}, (err) => {
	alert("failed");	
	});	*/  
  }

  viewImage(image) {
	  alert("http://skylimit.co.in/Client/ChatApplication/"+image);
     this.photoViewer.show("http://skylimit.co.in/Client/ChatApplication/"+image, 'title',);
  }

   imageDetail(image) {
		
      this.navCtrl.push('ImageDetailPage' , {"image": image});
  }

 presentToast() {
  let toast = this.toastCtrl.create({
    message: 'Image Uploded successfully!',
    duration: 3000,
    position: 'middle'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}

}
