import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManpowerPage } from './manpower';

@NgModule({
  declarations: [
    ManpowerPage,
  ],
  imports: [
    IonicPageModule.forChild(ManpowerPage),
  ],
})
export class ManpowerPageModule {}
