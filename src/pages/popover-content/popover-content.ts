import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, Platform, LoadingController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-popover-content',
  templateUrl: 'popover-content.html',
})
export class PopoverContentPage {
  loading: any;
  allProject: any;
  projectId: any;
  projectName: any;
  manpowerData: any;
  machineryData: any;
  sitediary: Array<{ProjectID:string,PhyPrg :string,FinPrg: string, CH : string ,AKK :string}>;
  sqlDBProjectData: any;
  public db: SQLiteObject;
    myDate: string = new Date().toISOString();
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public viewCtrl: ViewController, public authServiceProvider: AuthServiceProvider, private platform: Platform, private sqlite: SQLite, public loadingCtrl: LoadingController) {
	  platform.ready().then(() => {
        // this.getProjectList(); 
		 this.fatchDataFromSqlDB();
		 
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverContentPage');
  }

   showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Synchronise to Server');

    alert.addInput({
      type: 'checkbox',
      label: 'Site Progress',
      value: 'siteprogress',
     
    });

    alert.addInput({
      type: 'checkbox',
      label: 'Inspection ',
      value: 'inspection'
    });

	alert.addInput({
      type: 'checkbox',
      label: 'Punch List ',
      value: 'punchlist'
    });

    alert.addButton('Cancel');
	this.viewCtrl.dismiss();
    alert.addButton({
      text: 'Ok',
      handler: data => {
        console.log('Checkbox data:', data);
		this.viewCtrl.dismiss();
		 for(var i= 0; i<= data.length; i++) {
			  console.log(data[i]);
			if(data[i] == "siteprogress"){
			     this.getProjectList(); 
				 this.getRescData(); 
				 this.getMachRescData();
			}
			else if(data[i] == "inspection"){
			  console.log(2);
			}
			else if(data[i] == "punchlist"){
			  console.log(3);
			}
		  }		 	
      }
    });
    alert.present();

  }

  insertDataInSqlLiteDb() {
      
	  this.presentLoadingDefault();
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
	 // alert("records"+this.sitediary.length);
      for(var i= 0; i<=this.allProject.length; i++) {
		this.projectId = (this.allProject[i].ProjectID);
        this.projectName = (this.allProject[i].ProjectName);
		  
		     db.executeSql('INSERT INTO projects(ProjectID, ProjectName) VALUES(?, ?)', [this.projectId,  this.projectName])
		  .then(() =>  console.log('Executed SQL'))
		  .catch(e => alert(e));
		 
		  
       }
	      
	 }); 
  }

 getProjectList(){
    this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM projects", {})
        .then(() => alert('All records deleted'))
        .catch(e => console.log(e));
		
	 });
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.getProjectList(storage.getItem("l_username")).then((result) => {
	 
      this.loading.dismiss();
	  this.allProject = result;
	 this.insertDataInSqlLiteDb();

	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }

  getRescData(){
   this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM ManPowerResource", {})
        .then(() => alert('All records deleted'))
        .catch(e => console.log(e));
		
	 });
   var url="fType=RM";
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.manpowerData=result;
      this.insertRecDataInSqlLiteDb();
	}, (err) => {
     
	 // alert("err"+JSON.stringify(err));
    });
  }

  insertRecDataInSqlLiteDb(){
    
	 this.presentLoadingDefault();
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
	 // alert("records"+this.sitediary.length);
      for(var i= 0; i<=this.manpowerData.length; i++) {
		   db.executeSql('INSERT INTO ManPowerResource(ResourceID, resource) VALUES(?, ?)', [this.manpowerData[i].RID,  this.manpowerData[i].RDescription])
		  .then(() =>  console.log('Executed SQL'))
		  .catch(e => alert(e));
	   }
	      
	 }); 
  }
  getMachRescData(){
   this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	    }).then((db: SQLiteObject) => { 
		 db.executeSql("DELETE FROM MachineryResource", {})
        .then(() => alert('All records deleted'))
        .catch(e => console.log(e));
		
	 });
   var url="fType=MM";
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.machineryData=result;
      this.insertMechRecDataInSqlLiteDb();
	}, (err) => {
     
	 // alert("err"+JSON.stringify(err));
    });
  }
  insertMechRecDataInSqlLiteDb(){
    
	 this.presentLoadingDefault();
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => { 
	 // alert("records"+this.sitediary.length);
      for(var i= 0; i<=this.machineryData.length; i++) {
		   db.executeSql('INSERT INTO MachineryResource(ResourceID, resource) VALUES(?, ?)', [this.machineryData[i].MID,  this.machineryData[i].MDescription])
		  .then(() =>  console.log('Executed SQL'))
		  .catch(e => alert(e));
	   }
	      
	 }); 
  }
  presentLoadingDefault() {
	  let loading = this.loadingCtrl.create({
		content: 'Please wait...'
	  });

	  loading.present();

	  setTimeout(() => {
		loading.dismiss();
	  }, 5000);
	}

	fatchDataFromSqlDB() {
		this.sqlite.create({
		  name: 'projectList.db',
		  location: 'default'
	      }).then((db: SQLiteObject) => { 
		  db.executeSql('select * from projects', {}).then((data) => { 
		  this.sqlDBProjectData = [];

		  if(data.rows.length > 0) {
			for(var i = 0; i < data.rows.length; i++) {
			this.sqlDBProjectData.push({ProjectID: data.rows.item(i).ProjectID});
			}
		  }
		}, (err) => {
		   alert('Unable to execute sql: '+JSON.stringify(err));
		});
	  });	
	}


	showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }
}
