import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { BarcodeScanner,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { HomePage } from '../home/home';

/**
 * Generated class for the WebLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-web-login',
  templateUrl: 'web-login.html',
})
export class WebLoginPage {
 loading: any;
 scanData : string;
 encodeData : string ;
 encodedData : {} ;
options :BarcodeScannerOptions;
  constructor(public navCtrl: NavController,public barcodeScanner:BarcodeScanner ,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController) {
    this.scan();
  }
   scan(){
   this.options = {
        prompt : "Scan your barcode "
    }
    this.barcodeScanner.scan(this.options).then((barcodeData) => {

        console.log(barcodeData);
        this.scanData = barcodeData.text;
		//alert(barcodeData.text);
		this.setQRCode();
    }, (err) => {
        alert("Error occured : " + err);
    });         
 }
 encodeText(){
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE,this.scanData).then((encodedData) => {

        console.log(encodedData);
        this.encodedData = encodedData;

    }, (err) => {
        console.log("Error occured : " + err);
    });                 
  }  
  
  setQRCode(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.setQRCode("WA",storage.getItem("l_username"), this.scanData).subscribe(
     result => {
	  this.loading.dismiss();
	// alert(JSON.stringify(result));
	
	  this.navCtrl.setRoot(HomePage);
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+JSON.stringify(err));
    
    });
  } 
  
   showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   } 
}
