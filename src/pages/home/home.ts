import { Component } from '@angular/core';
import { NavController,Platform,PopoverController } from 'ionic-angular';
import { ChattabPage } from '../chattab/chattab';
import { PopoverContentPage } from '../popover-content/popover-content';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
 
 constructor(public platform: Platform,public navCtrl: NavController, public popoverCtrl: PopoverController) {
   
 }
 fabButtonClick() {
	   this.navCtrl.push('ChattabPage');
	}

	
presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverContentPage);
    popover.present({
      ev: myEvent
    });
  }

}


