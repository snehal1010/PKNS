import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the ProjecttabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projecttab',
  templateUrl: 'projecttab.html',
})
export class ProjecttabPage {
   loading: any;
  projectData:any;
     constructor(public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController) {
      this.getProjectList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjecttabPage');
  }
  getProjectList(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.getData("P",storage.getItem("l_username")).then((result) => {
	 
      this.loading.dismiss();
	  this.projectData = result;
	
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }

  showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }
}
