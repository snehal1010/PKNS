import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjecttabPage } from './projecttab';

@NgModule({
  declarations: [
    ProjecttabPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjecttabPage),
  ],
})
export class ProjecttabPageModule {}
