import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SiteFormPage } from './site-form';

@NgModule({
  declarations: [
    SiteFormPage,
  ],
  imports: [
    IonicPageModule.forChild(SiteFormPage),
  ],
})
export class SiteFormPageModule {}
