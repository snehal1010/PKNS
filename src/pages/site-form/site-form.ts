import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,ToastController  } from 'ionic-angular';
//import { FileChooser } from '@ionic-native/file-chooser';
//import { FilePath } from '@ionic-native/file-path';
//import { Transfer} from '@ionic-native/transfer';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
//import { SiteDiaryTabPage } from '../site-diary-tab/site-diary-tab';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the SiteFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-form',
  templateUrl: 'site-form.html',
 
})
export class SiteFormPage {

   siteProgressData = {IOID:'', Name:'', Company:'', VIN:'', VOUT:''};
	  nativepath: any;
	  public online:boolean = true;
	  tabParam = { project:'', date:''};

  constructor(public navCtrl: NavController, public navParams: NavParams, public authServiceProvider: AuthServiceProvider, public events:Events, private sqlite: SQLite, private network: Network, private toastCtrl: ToastController ) {
	if(navParams.get("item")!= null)
	  this.siteProgressData=navParams.get("item");
	//  alert(this.siteProgressData.Name);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SiteFormPage');
  }

  submitData() {

      let type = this.network.type;

		  if(type == "unknown" || type == "none" || type == undefined){
			this.presentToast();
			this.online = false;
			this.submitFormDataOnSqlLiteDb();
			
		  }else{
			console.log("The device is connected to internet!");
			this.online = true;
			this.submitFormDataOnServer();
		  }

   }
  submitFormDataOnServer() {
  
   var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");

   var url="fType=SSV&fName="+this.siteProgressData.Name+"&comments="+this.siteProgressData.Company+"&IN="+this.siteProgressData.VIN+"&OUT="+this.siteProgressData.VOUT+"&SDate="+date+"&PID="+project;
   //console.log(url);
    this.authServiceProvider.getURLData(url).subscribe(
     result => {
	 this.events.publish('reloadDetails');
	   this.navCtrl.pop();
	 
    },
    err =>{
      alert("Error : "+err);
    } ,
    () => {
     // alert('getData completed');
    }
  );
 }

 submitFormDataOnSqlLiteDb() {
  var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => {
		  db.executeSql('INSERT INTO visitors(ProjectID,dt,name, company, inData, outData) VALUES(?, ?, ?, ?, ?, ?)', [project,date,this.siteProgressData.Name,  this.siteProgressData.Company, this.siteProgressData.VIN, this.siteProgressData.VOUT])
		  .then(() => alert('Data Inserted in SQLLite DB'))
		  .catch(e => alert(e));
		  // this.events.publish('reloadDetails');
	       this.navCtrl.pop();
       });
   }
  /* store() {
    this.fileChooser.open().then((url) => {
      (<any>window).FilePath.resolveNativePath(url, (result) => {
        this.nativepath = result;
        alert(this.nativepath);
      }
      )
    })
  }*/

      
  /*  uploadFile() {
      this.fileChooser.open()
      {
	  (<any>window).FilePath.resolveNativePath(uri, (result) => {
        this.nativepath = result;
		 })
         const fileTransfer: TransferObject = this.transfer.create();

    // regarding detailed description of this you cn just refere ionic 2 transfer plugin in official website
      let options1: FileUploadOptions = {
         fileKey: 'file',
         fileName: this.nativepath.substr(this.nativepath.lastIndexOf('/') + 1),
         mimeType: "multipart/form-data",
         headers: {},
         params: {"app_key":"Testappkey"},
         chunkedMode : false
      
      }
	  alert(options1);
	  for(var key in options1) {
			var value = options1[key];
			alert(key);
			alert(value);
		}
		

      fileTransfer.upload(this.nativepath, 'your API that can take the required type of file that you want to upload.', options1)
       .then((data) => {
       // success
       alert("success"+JSON.stringify(data));
       }, (err) => {
       // error
       alert("error"+JSON.stringify(err));
           });

      })
      .catch(e => console.log(e));
  }*/

/*  upload() {
  this.fileChooser.open()
  .then(uri => console.log(uri))
  .catch(e => console.log(e));
  }*/


  presentToast() {
	let toast = this.toastCtrl.create({
	message: 'The device is not connected to internet!',
	duration: 3000,
	position: 'middle'
	});

	toast.onDidDismiss(() => {
	console.log('Dismissed toast');
	});

	toast.present();
   }


}
