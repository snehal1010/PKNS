import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController ,Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
/**
 * Generated class for the SiteDiaryTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-site-diary-tab',
  templateUrl: 'site-diary-tab.html',
})
export class SiteDiaryTabPage {
 public online:boolean = true;
  siteData: any;
  PhyPrg:string;
  FinPrg:string;
  CH:string;
  AKK:string;
  tabParam = { project:'', date:''};
  constructor(public events:Events,public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider,private sqlite: SQLite, private network: Network, public toastCtrl: ToastController) {
   //this. getSiteData();
    this.listenEvents();
  }

 listenEvents(){
   this.events.subscribe('reloadDetails',() => {
    //call methods to refresh content

	 this.getVisitorData();
   });
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad SiteDiaryTabPage');
  }
  fabButtonClick() {
 // alert(this.tabParam.project);
      if(this.tabParam.project === "")
	    alert("Select any project");
	  else
	   this.navCtrl.push('SiteFormPage',{item: null});
	}
  
  getSiteData(){
   var url="fType=FSD&PID="+this.tabParam.project+"&SDate="+this.tabParam.date;
   // console.log("site-dairy"+url);
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	 console.log(JSON.stringify(result));
	  this.PhyPrg=result.PhyPrg;
	  this.FinPrg=result.FinPrg;
	  this.CH=result.CH;
	  this.AKK=result.AKK;
     
	}, (err) => {
     
	  alert("err"+JSON.stringify(err));
    });
  }

 
   getVisitorData() {
       var storage = window.localStorage;
				 
	this.tabParam.date=storage.getItem("dt");
    this.tabParam.project= storage.getItem("project");
     this.authServiceProvider.getVisitorData(this.tabParam.project,this.tabParam.date).subscribe(
     result => {
	 //alert(JSON.stringify(result));
	  this.siteData = result;
     
	}, (err) => {
     
	  alert("err"+err);
    });
   }

   //function get called when tab get selected
   ionSelected() {
       // alert("Home Page has been selected");
	   var storage = window.localStorage;
     this.tabParam.project= storage.getItem("project");
	 this.tabParam.date=storage.getItem("dt");
     let type = this.network.type;

			  if(type == "unknown" || type == "none" || type == undefined){
			   // alert("not internet");
				this.presentToast();	
				this.online = false;
			//	this.getSqlLiteData(this.tabParam.project);
			  }else{
				//alert("The device is connected to internet!");
				this.online = true;
				 var storage = window.localStorage;
				 
				  this.tabParam.date=storage.getItem("dt");
				//  alert(this.tabParam.date);
				  this.getSiteData();
				  this.getVisitorData();
			  }
		
    //  alert("Home Page has been selected");
	 
	 // alert( this.tabParam.project);
    // do your stuff here
  }

  Save(){

  if(this.tabParam.project === "")
	    alert("Select any project");
  else{
   var storage = window.localStorage;
				 
	this.tabParam.date=storage.getItem("dt");
    this.tabParam.project= storage.getItem("project");
	 let type = this.network.type;
	if(type == "unknown" || type == "none" || type == undefined){
	        this.presentToast();
			this.online = false;
			this.submitFormDataOnSqlLiteDb();

	}else{
			var url="fType=SSD&PID="+this.tabParam.project +"&PhyPrg="+this.PhyPrg+"&FinPrg="+this.FinPrg+"&CH="+this.CH+"&AKK="+this.AKK+"&SDate="+this.tabParam.date;
			//alert(url);
			 this.authServiceProvider.getURLData(url).subscribe(
			 result => {
			// alert(JSON.stringify(result));
			  alert("Data saved");
     
			}, (err) => {
     
			  alert("err"+err);
			});
	}
   }
  }
 
  submitFormDataOnSqlLiteDb() {
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => {
		  db.executeSql('INSERT INTO site-diary(ProjectID , dt ,PhyPrg ,FinPrg ,CH ,AKK ) VALUES(?, ?, ?, ?, ?, ?)', [this.tabParam.project, this.tabParam.date, this.FinPrg, this.PhyPrg, ,this.CH,this.AKK])
		   .then(() => console.log('Data Inserted in SQLLite DB'))
		  .catch(e => console.log(e));
		 
       });
   }


   presentToast() {
	let toast = this.toastCtrl.create({
		message: 'The device is not connected to internet!',
		duration: 3000,
		position: 'middle'
	});

	toast.onDidDismiss(() => {
	 console.log('Dismissed toast');
	});

	toast.present();
   }

   Delete(item){
	 let index = this.siteData.indexOf(item);
   //  alert(index);

    if(index > -1){
      this.siteData.splice(index, 1);
    }
   }


   getDeleteVisitor(item) {
     let index = this.siteData.indexOf(item);
   //  alert(index);
     var url="fType=DSV&ItemID="+item.IOID;

	 alert(url);
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	// if(JSON.stringify(result) == "SUCCESS")
	 
	   this.siteData.splice(index, 1);
     
	}, (err) => {
     
	  alert("err"+err);
    });
   }


   EditVistor(item) {
     // alert(item.Name);
	   this.navCtrl.push('SiteFormPage',{item:item});
	}
}
