import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SiteDiaryTabPage } from './site-diary-tab';

@NgModule({
  declarations: [
    SiteDiaryTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SiteDiaryTabPage),
  ],
})
export class SiteDiaryTabPageModule {}
