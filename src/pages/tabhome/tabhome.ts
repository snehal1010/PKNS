import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,PopoverController,LoadingController } from 'ionic-angular';
import { ChattabPage } from '../chattab/chattab';
import { PopoverContentPage } from '../popover-content/popover-content';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
/**
 * Generated class for the TabhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabhome',
  templateUrl: 'tabhome.html',
})
export class TabhomePage {
in: boolean=true;
 out: boolean=true;
 intime: string;
 outtime: string;
 inColor: string="#03b6b3";
 outColor: string="#03b6b3";
 public timeNow:string= new Date().toISOString();
 punchtime:any;
 projectData: any;
 activityData :any;
 subactivityData :any;
 loading: any;
 hrs:any=[{"hr":"0"},{"hr":"1"},{"hr":"2"},{"hr":"3"},{"hr":"4"}];
 mins:any=[{"min":"0"},{"min":"15"},{"min":"30"},{"min":"45"}];
 hours:string;
 minitus:string;
 project: string;
 activity: string;
 subactivity: string;
 dis: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController) {
   this.hours="0";this.minitus="0";
   this.GetPunchTime(this.timeNow);
   this.getProjectList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabhomePage');
  }
   fabButtonClick() {
	   this.navCtrl.push('ChattabPage');
	}

	
presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverContentPage);
    popover.present({
      ev: myEvent
    });
  }

   punchInTime(){
  if(this.in == true){
  // alert(moment( this.intime ).format( 'h:mm a' ));
  
     this.inColor="#D3D3D3";
	 this.SetPunchTime("In",this.intime);
	
	 this.intime=moment(this.intime ).format( 'h:mm a' );
	}
  }
  punchOutTime(){
    if(this.out == true){
  // alert(moment( this.outtime ).format( 'h:mm a' ));
  
     this.outColor="#D3D3D3";
	 this.SetPunchTime("Out",this.outtime);
	
	 this.outtime=moment(this.outtime ).format( 'h:mm a' );
	}
  }
  SetPunchTime(type,time){
     var storage = window.localStorage;
    var uid= storage.getItem("l_username");
	
	  time= moment(time).format( 'DD/MM/YYYY' );
	  
	 // alert(dt.getHours()); 
   var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?"+"fType=PIN&empid=" +uid+ "&pdate=" + time + "&location=mumbai&pType=" + type;
   alert(url);
     this.authServiceProvider.setpunchTime(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	//  this.RescData=result;
     
	}, (err) => {
     
	  alert("err"+JSON.stringify(err));
    });
  }


  GetPunchTime(time){
     var storage = window.localStorage;
    var uid= storage.getItem("l_username");
	
	  time= moment(time ).format( 'DD/MM/YYYY' );//dd1+"/"+mm1+"/"+yr;
    var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?"+"fType=PT&empid=" +uid+ "&pdate=" + time;
  // alert(url);
     this.authServiceProvider.setpunchTime(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.punchtime=result;
      this.setTime();
	}, (err) => {
      this.intime=this.timeNow;
      this.outtime=this.timeNow;
	  this.outColor="#03b6b3";
	  this.out=true;
      this.inColor="#03b6b3";
	  this.in=true;
	 // alert("err"+JSON.stringify(err));
    });
  }

  setTime(){
    if(this.punchtime[0].InTime !=null){
	let inT=this.punchtime[0].InTime.split(" ");
	//alert(inT[2]);
	 this.inColor="#D3D3D3";
	 this.in=false;
	 this.intime=inT[2];
	 if(this.punchtime[0].OutTime !=null){
	   let outT=this.punchtime[0].OutTime.split(" ");
	   //alert(inT[2]);
	   this.outColor="#D3D3D3";
	   this.out=false;
	   this.outtime=outT[2];
	 }
	 else{
	    this.outtime=this.timeNow;
		this.outColor="#03b6b3";
	    this.out=true;
		}
	}else{
	 this.intime=this.timeNow;
     this.outtime=this.timeNow;
	}
  }

   getProjectList(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
	var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?fType=PROJ";
    this.authServiceProvider.setpunchTime(url).subscribe(
	  result => {
      this.loading.dismiss();
	  this.projectData = result;
	
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }
   changeProject(selectedValue: any){
    this.getActivityList(selectedValue);
   }

   getActivityList(pid){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
	var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?fType=ACT&pcode="+pid;
    this.authServiceProvider.setpunchTime(url).subscribe(
	  result => {
      this.loading.dismiss();
	  this.activityData = result;
	
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }

  changeActivity(selectedValue: any){
    this.getSubActivityList(selectedValue);
  }

   getSubActivityList(aid){
   
    this.showLoader("Loading Data...");
	var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?fType=SACT&aid="+aid;
    this.authServiceProvider.setpunchTime(url).subscribe(
	  result => {
      this.loading.dismiss();
	  this.subactivityData = result;
	
	}, (err) => {
      this.loading.dismiss();
	
	  alert("err"+err);
    
    });
  }


  

  showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }

   changedate(dt:any){
     // alert(this.timeNow);
	 this.GetPunchTime(this.timeNow);
   
   }
   Save(){
    	
	 var time= moment(this.timeNow).format( 'DD/MM/YYYY' );//dd1+"/"+mm1+"/"+yr;
    var storage = window.localStorage;
    var uid= storage.getItem("l_username");
	 this.showLoader("Wait...");
     var url="http://103.253.109.23/klctimesheet/mobile/webservice.aspx?fType=WD&empid=" + uid + "&pdate=" + time+"&project="+this.project+"&sproject="+this.activity+"&activity="+this.subactivity+"&desc="+this.dis+"&hrs="+this.hours+"&min="+this.minitus;
  // alert(url);
   this.authServiceProvider.setpunchTime(url).subscribe(
	  result => {
      this.loading.dismiss();
	//  alert(result);
	
	}, (err) => {
      this.loading.dismiss();
	
	 // alert("err"+err);
    
    });
   }
}
