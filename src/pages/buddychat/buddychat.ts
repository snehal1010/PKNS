import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, LoadingController, ToastController,Platform } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Http, Headers } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { ActionSheetController } from 'ionic-angular'
import { FCM } from '@ionic-native/fcm';
import 'rxjs/add/operator/map';
import * as moment from 'moment';
/**
 * Generated class for the BuddychatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
  declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-buddychat',
  templateUrl: 'buddychat.html',
})
export class BuddychatPage {
   @ViewChild('content') content: Content;
  user: any;
  data: any;
  task: any;
  userId: any;
   oldMessage: any;
  allMessage: any;
  message: any;
  loading: any;	
  friendsUserId: any;
  userData = {userId: 'this.userId'};
  messageData = { message :'Yes', userId: '11', friendsUserId: '10'};
  base64Image:any;
  public imageUri: any;
  public uploadImage: any;
  scroll = 0;
  lastImage: string = null;
  oldlength=0;
  groupdate:any;
  constructor(private fcm: FCM,public navCtrl: NavController, public navParams: NavParams,
    public events: Events, public zone: NgZone, public loadingCtrl: LoadingController,
    public http: Http, public authServiceProvider: AuthServiceProvider, private toastCtrl: ToastController, public camera:Camera,public actionSheetCtrl: ActionSheetController, private transfer: Transfer, private file: File, private filePath: FilePath,public platform: Platform) {

    this.user = this.navParams.get("user");
	
    console.log(this.user);
    this.friendsUserId =  this.user.UID;
	this.friendsUserId = this.friendsUserId.toLowerCase();
    this.userId = this.navParams.get("userId").toLowerCase();
   // alert(this.friendsUserId);
  
	this.getMessage();
	this.task = setInterval( () => {
	  this.getMessage();
	}, 300);
	
  }

   addmessage() {
   /* let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
	let data=JSON.stringify({'userId':this.userId, 'friendsUserId':this.friendsUserId, 'message':this.message, 'image':null});
	this.http.post('http://skylimit.co.in/Client/ChatApplication/storeChat.php?',data,{headers: headers})
	.map(res => res.json())
	.subscribe(res => {
          this.message = '';
	}, (err) => {
	alert("failed");	
	});*/

	var url="fType=SUM&uids="+this.userId +"&uid="+this.friendsUserId +"&comments="+this.message+"&mType=T";
	var storage = window.localStorage;
    //this.showLoader("Loading Data...");
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	   this.message = '';
	}, (err) => {
  
	  alert("err"+JSON.stringify(err));
    });
   }

    getMessage() {
    /* this.http.get('http://skylimit.co.in/Client/ChatApplication/getChat.php?'+'userId='+this.userId+'&friendsUserId='+this.friendsUserId)
	.map(res => res.json())
	.subscribe(res => {
	this.allMessage = res.data;
	//alert(JSON.stringify(this.allMessage));
	 this.content.scrollToBottom();
	}, (err) => {
	alert("failed");
	});
    }*/
	var url="fType=UM&uids="+this.userId +"&uid="+this.friendsUserId ;
	
    this.authServiceProvider.getURLData(url).subscribe(
     result => {
	
	 this.allMessage = result;
	//alert(JSON.stringify(this.allMessage));
	// this.content.scrollToBottom();
	 if(this.scroll<2) {
        this.content.scrollToBottom();
        this.scroll +=1;
      }
	}, (err) => {
  
	 // alert("err"+JSON.stringify(err));
    });
  }
    ionViewDidEnter() {
    //this.getMessage();
    }
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  scrollto() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 1000);
  }

openActionSheet(){
 console.log('opening');
 let actionsheet = this.actionSheetCtrl.create({
 title:"Choose Album",
 buttons:[{
 text: 'Camera',
 handler: () => {
  this.takePicture(this.camera.PictureSourceType.CAMERA);
 console.log("Camera Clicked");
 //this.takePicture();
 }
 },{
 text: 'Gallery',
 handler: () =>{
// alert("Gallery Clicked");
// this.openGallery();
  this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
 }
 }]
 });
 actionsheet.present();
}


public takePicture(sourceType) {
  // Create options for the Camera Dialog
  var options = {
    quality: 100,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation: true
  };
 
  // Get the data of an image
  this.camera.getPicture(options).then((imagePath) => {
   //alert(imagePath);
    // Special handling for Android library
    if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
	   
      this.filePath.resolveNativePath(imagePath)
        .then(filePath => {
		  alert(filePath);
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
		 
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }, (err) => {
    alert(JSON.stringify(err));});
    } else {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
	   
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
	
  }, (err) => {
    alert('Error while selecting image.');
  });
}

 // Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
 
// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
	//alert("file name"+namePath +" dir"+cordova.file.dataDirectory);
	this.base64Image=namePath+currentName;
	//this.uploadImageOnserver();
	//this.fileChange;
  }, error => {
    alert('Error while storing file.');
  });
}
 

 
// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImageOnserver() {
var win = function (r) {
        alert("Code = " + r.responseCode);
        alert("Response = " + r.response);
        alert("Sent = " + r.bytesSent);
    };

    var fail = function (error) {
        alert("An error has occurred: Code = " + error.code);
        alert("upload error source " + error.source);
        alert("upload error target " + error.target);
    };

var storage = window.localStorage;
 
  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);
 
  // File name only
  var filename = this.lastImage;
  // Destination URL
  var url = "http://103.253.109.23/pkns.web.r2/mobile/webservice?fType=PP&uid=" +storage.getItem("l_username")+"&fName="+filename;

  var options  = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "image/jpeg",
    headers: {}
	
  };
 
  const fileTransfer: TransferObject = this.transfer.create();
 
  this.loading = this.loadingCtrl.create({
    content: 'Uploading...',
  });
  this.loading.present();
 
  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath,url, options).then(data => {
    this.loading.dismissAll()
    alert('Image succesful uploaded.');
  }, err => {
    this.loading.dismissAll()
   alert(JSON.stringify(err));
  });
}


   presentLoadingDefault() {
	  let loading = this.loadingCtrl.create({
		content: 'Please wait image is uploading...'
	  });

	  loading.present();

	  setTimeout(() => {
		loading.dismiss();
	  }, 5000);
	}

	callFunction(){
     if(this.scroll>2) {
        this.content.scrollToBottom();
        this.scroll +=1;
    }
   }
    oldD: string;
	NewD: string;
    
	checkDate(oldDate){
	 
	  oldDate=oldDate.replace("/Date(" ,"");
	  oldDate=oldDate.replace(")/" ,"");
	  this.groupdate= new Date(Number(oldDate));
	  this.NewD= moment(this.groupdate).format('DD/MM/YYYY' );
	  if(this.NewD === this.oldD){
	      this.oldD=this.NewD;
		 return false;
      }
	  else{
	    this.oldD=this.NewD;
	    return true;
	  }
	}
}