import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, ToastController, Platform  } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
//import { HomePage } from '../home/home';
//import { TabhomePage } from '../tabhome/tabhome';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
declare var FCMPlugin: any;
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
 public hide: boolean =true;
 devicetoken: any;
  loading: any;
    data: any;

  loginData = { user:'', pass:'',imei:'','token':'' };
 
  constructor(public authServiceProvider: AuthServiceProvider,private uniqueDeviceID: UniqueDeviceID,public platform: Platform,public navParams: NavParams,public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  var storage = window.localStorage;
      var storage = window.localStorage;
	  this.loginData.user=storage.getItem("l_username");
	  this.loginData.pass=storage.getItem("l_pass");
	  this.loginData.imei=storage.getItem("imei");
  
     if(storage.getItem("activate")=== "false"){
	   this.hide=false;
     }
	 else{
	     this.presentToast("User is not activated.");
	 }
    this.platform.ready().then(() => {
	   this.uniqueDeviceID.get()
          .then((uuid: any) =>  storage.setItem("imei",uuid))
          .catch((error: any) => alert(error));
          
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signin() {
 
	var storage = window.localStorage;
	storage.setItem("l_username", this.loginData.user);
	storage.setItem("l_pass", this.loginData.pass);
	storage.setItem("login", "true");
	
	window.location.reload();
  }

 activate(){
    var storage = window.localStorage;
	 /* this.loginData.user=storage.getItem("l_username");
	  this.loginData.pass=storage.getItem("l_pass");
	  this.loginData.imei=storage.getItem("imei");*/
    this.showLoader();
    FCMPlugin.getToken(
	  (t) => {
		console.log(t);
		this.loginData.token = t;
		//alert(this.loginData.token);
	     this.authServiceProvider.activation(this.loginData).then((result) => {
	 
      this.loading.dismiss();
	
      this.data = result;
	    //alert(this.data.RStatus);
      if(this.data.RStatus== true){
	     this.presentToast("Activation detials has been sent.");
		 storage.setItem("activate", "true");
		  this.hide=true;
      }
      else{
	     storage.setItem("activate", "true");
         this.presentToast("Data Already Present.");
		  this.hide=true;
      }
    }, (err) => {
      this.loading.dismiss();
	  // this.rootPage=LoginPage; 
	  alert(err);
    //  this.presentToast(err);
    });
	  },
	  (e) => {
		console.log(e);
	  }
	);
 }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Sending Data...'
    });

    this.loading.present();
   }

    presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true
    });
	}
}
