import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
/**
 * Generated class for the MyprojectsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-myprojects',
  templateUrl: 'myprojects.html',
})
export class MyprojectsPage {
    public projectData: any = [];
	 loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController) {
    	  this.getProjectList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyprojectsPage');
  }
  getProjectList(){
    var storage = window.localStorage;
    this.showLoader("Loading Data...");
    this.authServiceProvider.getProjectList(storage.getItem("l_username")).then((result) => {
	 
      this.loading.dismiss();
	  this.projectData = result;
	  this.changeDate();
	}, (err) => {
      this.loading.dismiss();
	
	  alert("Error"+"Check your internet connection");
    
    });
  }

  changeDate(){
  var i;
    for(i=0;i<this.projectData.length;i++){
	 this.projectData[i].StartDate=moment( this.projectData[i].StartDate ).format( 'YYYY-MM-DD' );
	  this.projectData[i].FinishDate=moment( this.projectData[i].FinishDate ).format( 'YYYY-MM-DD' );
	
	}
  }
    showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }

}
