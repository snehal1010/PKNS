import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events,ToastController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the MaterialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-material',
  templateUrl: 'material.html',
})
export class MaterialPage {
    RescData:any;
    materialData ={resource:'', brand:'',qty:'', remark:''};
    public online:boolean = true; 

  constructor(public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider, private sqlite: SQLite, private network: Network, private toastCtrl: ToastController,  public events:Events) {
 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterialPage');
  }

  submitData() {
     var storage = window.localStorage;
				 
	
    var project= storage.getItem("project");

   if(project === "")
	    alert("Select any project");
	  else{
      let type = this.network.type;

		  if(type == "unknown" || type == "none" || type == undefined){
			this.presentToast();
			this.online = false;
			this.submitFormDataOnSqlLiteDb();
			
		  }else{
			console.log("The device is connected to internet!");
			this.online = true;
			this.submitFormDataOnServer();
		  }
       }
  }

  submitFormDataOnServer() {
  
   var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");
	var uid= storage.getItem("l_username");
   var url="fType=SST&PID="+project+"&CH="+this.materialData.resource+"&Qty="+this.materialData.qty+"&AKK="+this.materialData.brand+"&comments="+this.materialData.remark+"&SDate="+date+"&uid="+uid;
   alert(url);
    this.authServiceProvider.getURLData(url).subscribe(
     result => {
	 this.events.publish('reloadDetails');
	   this.navCtrl.pop();
	 
    },
    err =>{
      alert("Error : "+err);
    } ,
    () => {
     // alert('getData completed');
    }
  );
 }

 submitFormDataOnSqlLiteDb() {
    var storage = window.localStorage;
				 
	var date=storage.getItem("dt");
    var project= storage.getItem("project");
	  this.sqlite.create({
		name: 'projectList.db',
		location: 'default'
	  }).then((db: SQLiteObject) => {
		  db.executeSql('INSERT INTO Material(ProjectID,dt,resource,brand, Qty, remark) VALUES(?, ?, ?, ?,?, ?)', [project,date,this.materialData.resource,this.materialData.brand,  this.materialData.qty, this.materialData.remark])
		  .then(() => alert('Data Inserted in SQLLite DB'))
		  .catch(e => alert(e));
		
	       this.navCtrl.pop();
       });
   }
  presentToast() {
	let toast = this.toastCtrl.create({
	message: 'The device is not connected to internet!',
	duration: 3000,
	position: 'middle'
	});

	toast.onDidDismiss(() => {
	console.log('Dismissed toast');
	});

	toast.present();
  }
}
