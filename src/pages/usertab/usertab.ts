import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,App,Platform ,AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Push , PushObject, PushOptions } from '@ionic-native/push';
/**
 * Generated class for the UsertabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usertab',
  templateUrl: 'usertab.html',
})
export class UsertabPage {
// UserData:any;
 loading: any;

  user: any;
 // user_Id: any;
  userId: any;
  friendId: any;
  friendDetails: any;
  devicetoken: any;
  token: any;
  temparr = [];
  filteredusers = [];
  filtereduserscp = [];
  deviceData = { devicetoken :'', userId : ''};
  userData: any;
  notificationId: any;
  data: any;
  notid: any;
 
  constructor(private app:App,public navCtrl: NavController, public navParams: NavParams,public authServiceProvider: AuthServiceProvider,public loadingCtrl: LoadingController, private platform: Platform, public push: Push, public alertCtrl: AlertController)
  {
    platform.ready().then(() => {
     this.push.hasPermission()
		  .then((res: any) => {

			if (res.isEnabled) {
			  alert('We have permission to send push notifications');
			} else {
			  alert('We do not have permission to send push notifications');
			}

		  });
		  this.initPushNotification();
     });

		 this.getUserList(); 	
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsertabPage');
  }

  getUserList(){
  
    var storage = window.localStorage;
	this.userId= storage.getItem("l_username");
    this.showLoader("Loading Data...");
	var url="fType=UL";
    this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.userData=result;
	  this.filtereduserscp = result;
      this.loading.dismiss();
	}, (err) => {
    this.loading.dismiss();
	  alert("err"+JSON.stringify(err));
    });
  }

  showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });
    this.loading.present();
  }


  buddychat(user) {
    var storage = window.localStorage;
	
       this.app.getRootNav().push('BuddychatPage' , {"user": user, "userId": storage.getItem("l_username")});
	  // this.navCtrl.push('BuddychatPage' , {"user":user, "userId": storage.getItem("l_username")});
  }

  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '305973352869'
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

   /* pushObject.on('registration').subscribe((data: any) => {
	   this.deviceData = { devicetoken : data.registrationId, userId : this.userId};
		this.authServiceProvider.saveDeviceToken(this.deviceData).then((result) => {
		}, (err) => {
		alert("failed");	
		});	  
    });*/

    pushObject.on('notification').subscribe((data: any) => {
	  alert(JSON.stringify(data.additionalData));
	  this.notificationId = {userId: data.additionalData.friendId}
	  this.notid = {userid: data.additionalData.friendId, name:data.additionalData.friendName}
	 // this.notid = data.additionalData.friendId;
	  //this.notificationId = data.additionalData.friendId;

	  /*this.authServiceProvider.getUserDetails(this.notificationId).then((result) => {
	   
      this.userData = result;
	  
	  alert(JSON.stringify(this.userData));
	  
		}, (err) => {
		   alert("Error");
		});*/
	  
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = this.alertCtrl.create({
          title: 'New Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              this.navCtrl.push('BuddychatPage' , {"user":  this.notid,"userId": this.userId});
            }
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        this.navCtrl.push('BuddychatPage' , {"user": this.notid, "userId":this.userId});
        alert('Push notification clicked');
      }
    });

    pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
  }


  searchuser(searchbar: any) {
    this.userData = this.filtereduserscp;
    var q = searchbar.target.value;
   
		if (q && q.trim() != '') {
      this.userData = this.userData.filter((user) => {
        return (user.UName.toLowerCase().indexOf(q.toLowerCase()) > -1);
      })
    }
  }
}
