import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaterialResourcePage } from './material-resource';

@NgModule({
  declarations: [
    MaterialResourcePage,
  ],
  imports: [
    IonicPageModule.forChild(MaterialResourcePage),
  ],
})
export class MaterialResourcePageModule {}
