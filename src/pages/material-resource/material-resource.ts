import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,ToastController,LoadingController,Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the MaterialResourcePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-material-resource',
  templateUrl: 'material-resource.html',
})
export class MaterialResourcePage {
 public online:boolean = true;
  loading: any;
  ManPower : any;
  Machinery  : any;
  Material  : any;
  tabParam = { project:'', date:''};
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,public authServiceProvider: AuthServiceProvider, private network: Network,public toastCtrl: ToastController,public loadingCtrl: LoadingController, public events:Events) {
  // this.getManPower();
 //  this.getMachinery();
    this.listenEvents();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterialResourcePage');
  }
  listenEvents(){
   this.events.subscribe('reloadDetails',() => {
    //call methods to refresh content

	this.getManPower();
    this.getMachinery();
    this.getMaterial();
	
   });
}
  getManPower(){
     var storage = window.localStorage;
     this.tabParam.project= storage.getItem("project");
	 this.tabParam.date=storage.getItem("dt");
     
	 var url="fType=RF&PID="+this.tabParam.project+"&SDate="+this.tabParam.date;
	// alert(url);
	 this.showLoader('wait...');
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	// alert(JSON.stringify(result));
	  this.ManPower=result;
      this.loading.dismiss();
	}, (err) => {
	  this.ManPower=[];
      this.loading.dismiss();
	 // alert("err"+JSON.stringify(err));
    });
  
  }

  getMachinery (){

  var storage = window.localStorage;
     this.tabParam.project= storage.getItem("project");
	 this.tabParam.date=storage.getItem("dt");
  
	 var url="fType=MF&PID="+this.tabParam.project+"&SDate="+this.tabParam.date;
	// alert(url);
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	    ///alert(JSON.stringify(result));
	  this.Machinery=result;
   
	}, (err) => {
    
	 // alert("err"+JSON.stringify(err));
    });
  }

  getMaterial(){
     var storage = window.localStorage;
     this.tabParam.project= storage.getItem("project");
	 this.tabParam.date=storage.getItem("dt");
     
	 var url="fType=TF&PID="+this.tabParam.project+"&SDate="+this.tabParam.date;
	 alert(url);
	// this.showLoader('wait...');
     this.authServiceProvider.getURLData(url).subscribe(
     result => {
	 alert(JSON.stringify(result));
	  this.Material=result;
     // this.loading.dismiss();
	}, (err) => {
	  this.Material=[];
    //  this.loading.dismiss();
	  alert("err"+JSON.stringify(err));
    });
  
  }
   manPowersPrompt() {
   /* let prompt = this.alertCtrl.create({
	  title: 'Man Power',
      message: "Enter the following details",
      inputs: [
			{
          name: 'Qty',
          placeholder: 'Qty'
        },
       {
          name: 'Remark',
          placeholder: 'Remark'
        },
			{
          name: 'Resource',
          placeholder: 'Resource'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();*/
	this.navCtrl.push('ManpowerPage');
  }

   machineryPrompt() {
    /*let prompt = this.alertCtrl.create({
	  title: 'Machinery',
      message: "Enter the following details",
      inputs: [
			{
          name: 'Qty',
          placeholder: 'Qty'
        },
       {
          name: 'Remark',
          placeholder: 'Remark'
        },
			{
          name: 'Resource',
          placeholder: 'Resource'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();*/
	 this.navCtrl.push('MachineryPage');
  }

   materialPrompt() {
   /* let prompt = this.alertCtrl.create({
	  title: 'Material',
      message: "Enter the following details",
      inputs: [
        {
          name: 'Description',
          placeholder: 'Description'
        },
       {
          name: 'Brand',
          placeholder: 'Brand'
        },
			{
          name: 'Qty',
          placeholder: 'Qty'
        },
        {
          name: 'Remark',
          placeholder: 'Remark'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();*/

	this.navCtrl.push('MaterialPage');
  }

    //function get called when tab get selected
   ionSelected() {
        var storage = window.localStorage;
     this.tabParam.project= storage.getItem("project");
	 this.tabParam.date=storage.getItem("dt");
     let type = this.network.type;

			  if(type == "unknown" || type == "none" || type == undefined){
			   // alert("not internet");
				this.presentToast();	
				this.online = false;
			//	this.getSqlLiteData(this.tabParam.project);
			  }else{
				//alert("The device is connected to internet!");
				this.online = true;
				 var storage = window.localStorage;
				 
				  this.tabParam.date=storage.getItem("dt");
				 // alert(this.tabParam.date);
				  this.getManPower();
				  this.getMachinery();
				  this.getMaterial();
			  }
   }


   presentToast() {
	let toast = this.toastCtrl.create({
		message: 'The device is not connected to internet!',
		duration: 3000,
		position: 'middle'
	});

	toast.onDidDismiss(() => {
	 console.log('Dismissed toast');
	});

	toast.present();
   }

   showLoader(text){
    this.loading = this.loadingCtrl.create({
        content: text
    });

    this.loading.present();
   }

}
